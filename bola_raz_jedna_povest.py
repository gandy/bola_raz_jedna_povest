#!/usr/bin/env python3

# Copyright 2019 GandY
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

from pyquery import PyQuery as pq
import re
import unicodedata
import urllib.request
import os
import mutagen
from mutagen.easyid3 import EasyID3
import json

BASE_URL = 'http://slovensko.rtvs.sk'
SHOW_ADDRESS = '/relacie/bola-raz-jedna-povest'
PAGE_PART = '?page='

DOWNLOAD_DIR = './Bola_raz_jedna_povest'

# ID3
AUTHOR = 'RTVS'
ARTIST = 'RTVS'
ALBUM = 'Bola raz jedna povest'
GENRE = 'Podcast'

def add_id3_tag(filename, **options):
    keys = [ 'album', 'title', 'artist', 'tracknumber', 'author', 'genre' ]
    options['album'] = ALBUM
    options['artist'] = ARTIST
    options['author'] = AUTHOR
    options['genre'] = GENRE

    try:
        meta = EasyID3(filename)
    except mutagen.id3.ID3NoHeaderError:
        meta = mutagen.File(filename, easy=True)
        meta.add_tags()

    for key in keys:
        if not (key in meta):
            meta[key] = options[key]

    meta.save()

count = 0
while True:
    count = count + 1
    print('Page: ' + str(count))
    print('==========')
    page = pq(url = BASE_URL + SHOW_ADDRESS + PAGE_PART + str(count))
    if not page('.article'):
        break

    for link in page('.article').items():
        title = str(link('a.article__image').attr['title'])
        episode_url = str(link('a.article__image').attr['href'])
        perex = bytes.decode(re.sub(r'\s', ' ', unicodedata.normalize('NFD', link('p.article__perex').text())).encode('ascii', 'ignore')) if link('p.article__perex').text() else 'Bola raz jedna povest.'
        if re.search('/bola-raz-jedna-povest-.*-cast', episode_url):
            number = ("{:03d}".format(int(re.findall(r'\b\d+\b', title)[0])))
            name = re.sub(r'_{1,}','_',
                    re.sub('_-_', '-',
                     re.sub(r'[^a-zA-Z0-9_-]', '',
                      re.sub(r'\s', '_',
                       re.sub(r'(.[a-z])([.?!;][\s]*[A-Z].*)', r'\1',
                        re.sub('\.\.\.', '.',
                         re.sub('^\.\.\.', '', perex[0:122]))))))) 
            file_name = DOWNLOAD_DIR +\
                        '/' +\
                        str(number) +\
                        '-' +\
                        name +\
                        '.mp3'
            sub_page = pq(url = BASE_URL + episode_url)
            id = str(sub_page('[id*="player_audio"]').attr('id')).rsplit('_',1)[1]
            mp3_url =json.loads(urllib.request.urlopen('https://www.rtvs.sk/json/audio5f.json?id=' + id).read())['playlist'][0]['sources'][0]['src']
            if not os.path.isdir(DOWNLOAD_DIR):
                print('Making directory - ' + DOWNLOAD_DIR)
                os.makedirs(DOWNLOAD_DIR)

            if os.path.isfile(file_name):
                print('File ' + file_name + ' exists ... skiping')
            else:
                print('File ' + file_name + ' is downloading')
                urllib.request.urlretrieve(mp3_url, file_name)

            add_id3_tag(file_name, title = re.sub('_', ' ', name), tracknumber = number)

