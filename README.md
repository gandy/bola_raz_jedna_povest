# bola_raz_jedna_povest

## description
Python script for downloading mp3 archive for show 'Bola raz jedna povest' from RTVS.

## dependencies
* python3
* pyquery
* mutagen

## Usage
Script download all episodes to direcotry ./Bola_raz_jedna_povest. Edit variable DOWNLOAD_DIR in script bola_raz_jedna_povest.py. Or use symlink.
